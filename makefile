# Front end commands
front-install:
	cd front-end && \
	npm install

front-launch:
	cd front-end && \
	npm start

front-build:
	cd front-end && \
	npm build

docker-build: 
	cd front-end && \
	docker build . -t welikesportz/node_web_app

docker-run:
	cd front-end && \
	docker run -it --rm -p 3001:3000 -e CHOKIDAR_USEPOLLING=true welikesportz/node_web_app

gui-tests:
	python front-end/gui_tests/guitests.py