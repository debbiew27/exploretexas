import os
from sys import platform

if __name__ == "__main__":
    if platform == "win32":
        PATH = "./front-end/gui_tests/chromedriver.exe"
    elif platform == "linux":
        PATH = "./front-end/gui_tests/chromedriver_linux"
    else:
        print("Unsupported OS")
        exit(-1)

    os.system("python ./front-end/gui_tests/modelTests.py " + PATH)
    os.system("python ./front-end/gui_tests/navbarTests.py " + PATH)
    os.system("python ./front-end/gui_tests/splashTests.py " + PATH)
    os.system("python ./front-end/gui_tests/aboutTests.py " + PATH)
