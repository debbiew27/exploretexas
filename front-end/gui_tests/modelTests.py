from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.chrome.service import Service

import unittest
import sys

URL = "https://www.welikesportz.me/cities/"


class ModelTests(unittest.TestCase):
    def setUp(cls):
        service = Service(PATH)
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--disable-dev-shm-usage")
        cls.driver = webdriver.Chrome(service=service, options=chrome_options)
        cls.driver.get(URL)
        cls.actions = ActionChains(cls.driver)

    def tearDown(cls):
        cls.driver.quit()

    def testSearch(self):
        self.driver.get(URL)
        try:
            WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.ID, "search-bar"))
            )
        except Exception as ex:
            print("Couldn't find search bar: " + str(ex))
            return

        self.driver.find_element(By.ID, "search-bar").send_keys("cl" + Keys.ENTER)

        total = ""

        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.CLASS_NAME, "ant-card")))
        element = self.driver.find_element(By.CLASS_NAME, "ant-pagination")
        total = element.find_element(By.CLASS_NAME, "ant-pagination-total-text").text
        if total == "Total 76 items":
            return self.testSearch()

        self.assertEqual(total, "Total 3 items")

    def testSelect(self):
        self.driver.get(URL)
        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.ID, "name"))
            )
        except Exception as ex:
            print("Couldn't find Sort by Name select: " + str(ex))
            return
        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.ID, "state"))
            )
        except Exception as ex:
            print("Couldn't find Filter by State select: " + str(ex))
            return


if __name__ == "__main__":
    PATH = sys.argv[1]
    unittest.main(argv=["first-arg-is-ignored"])
