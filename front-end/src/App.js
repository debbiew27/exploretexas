import React from "react";
import Navigation from "./components/navigation/Navigation";
import "bootstrap/dist/css/bootstrap.min.css";
import styles from "./App.module.css";

export default function App() {
  return (
    <div className={styles.container}>
      <Navigation></Navigation>
    </div>
  );
}
