import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import Home from "../splash/Home";
import About from "../about/About";
import Cities from "../cities/Cities.js";
import Players from "../players/Players.js";
import Teams from "../teams/Teams.js";
import Search from "../search/SearchPage";
import Visualizations from "../visualizations/Visualizations";

import CitiesInstance from "../cities/CitiesInstance";
import PlayersInstance from "../players/PlayerInstance";
import TeamsInstance from "../teams/TeamsInstance";

import styles from "./Navigation.module.css";

import { Menu } from "antd";
import { QueryParamProvider } from "use-query-params";

export default function Navigation() {
  return (
    <Router>
      <QueryParamProvider ReactRouterRoute={Route}>
        <div className={styles.navB}>
          <div className={styles.navbarContainer}>
            <div className={styles.leftMenu}>
              <Link to="/">
                <img
                  className={styles.logo}
                  src={"./logo.png"}
                  alt="Our Logo"
                />
              </Link>
            </div>
            <div className={styles.rightMenu}>
              <Menu mode="horizontal" theme="light">
                <Menu.Item key="About">
                  <Link style={{ textDecoration: "none" }} to="/about">
                    About
                  </Link>
                </Menu.Item>
                <Menu.Item key="Cities">
                  <Link style={{ textDecoration: "none" }} to="/cities">
                    Cities
                  </Link>
                </Menu.Item>
                <Menu.Item key="Players">
                  <Link style={{ textDecoration: "none" }} to="/players">
                    Players
                  </Link>
                </Menu.Item>
                <Menu.Item key="Teams">
                  <Link style={{ textDecoration: "none" }} to="/teams">
                    Teams
                  </Link>
                </Menu.Item>
                <Menu.Item key="Search">
                  <Link style={{ textDecoration: "none" }} to="/search">
                    Search
                  </Link>
                </Menu.Item>
                <Menu.Item key="Visualizations">
                  <Link style={{ textDecoration: "none" }} to="/visualizations">
                    Visualizations
                  </Link>
                </Menu.Item>
              </Menu>
            </div>
          </div>
        </div>
        <div className={styles.bodyContainer}>
          <Switch>
            <Route path="/about">
              <About />
            </Route>
            <Route path="/cities/:id">
              <CitiesInstance />
            </Route>
            <Route path="/players/:id">
              <PlayersInstance />
            </Route>
            <Route path="/teams/:id">
              <TeamsInstance />
            </Route>
            <Route path="/cities">
              <Cities />
            </Route>
            <Route path="/players">
              <Players />
            </Route>
            <Route path="/teams">
              <Teams />
            </Route>
            <Route path="/search">
              <Search />
            </Route>
            <Route path="/visualizations">
              <Visualizations />
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </div>
      </QueryParamProvider>
    </Router>
  );
}
