export function filterInfo({ name, state, proteams, sports, population }) {
  let filterQuery = {};

  if (name === "A-Z") {
    filterQuery["order_by"] = [{ field: "cities_name", direction: "asc" }];
  } else if (name === "Z-A") {
    filterQuery["order_by"] = [{ field: "cities_name", direction: "desc" }];
  }

  if (proteams === "Asc.") {
    filterQuery["order_by"] = [{ field: "cities_numsports", direction: "asc" }];
  } else if (proteams === "Desc.") {
    filterQuery["order_by"] = [
      { field: "cities_numsports", direction: "desc" },
    ];
  }

  if (population === "Asc.") {
    filterQuery["order_by"] = [
      { field: "cities_population", direction: "asc" },
    ];
  } else if (population === "Desc.") {
    filterQuery["order_by"] = [
      { field: "cities_population", direction: "desc" },
    ];
  }

  let filterList = [];
  if (state.length > 0) {
    filterList.push({
      name: "cities_state",
      op: "in",
      val: state,
    });
  }
  if (sports.length > 0) {
    for (let i = 0; i < sports.length; i++) {
      filterList.push({
        name: "cities_popularsports",
        op: "ilike",
        val: "%" + sports[i].substring(1) + "%",
      });
    }
  }
  filterQuery["filters"] = filterList;
  return JSON.stringify(filterQuery);
}
