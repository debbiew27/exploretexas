import React from "react";
import { useHistory } from "react-router-dom";
import styles from "./Cities.module.css";
import { Card, Typography } from "antd";
import Highlighter from "react-highlight-words";
import Row from "react-bootstrap/Row";

const { Title, Paragraph } = Typography;

export default function CitiesSearch({
  data,
  searchQuery,
  link,
  total,
  numResults,
}) {
  const history = useHistory();

  return (
    <div>
      {total === -numResults ? (
        <Card className={styles.card} hoverable={true}>
          <Title id="no-results" level={3}>
            {" "}
            No results found
          </Title>
        </Card>
      ) : (
        <Row xs={1} md={3} className="d-flex justify-content-center g-3">
          {data.map((city) => (
            <Card
              className={styles.card}
              hoverable={true}
              onClick={() => history.push("cities/" + city.id)}
            >
              <Title level={3}>
                {
                  <Highlighter
                    searchWords={searchQuery.split(" ")}
                    textToHighlight={city.cities_name}
                  />
                }{" "}
              </Title>
              <Paragraph>
                <Highlighter
                  searchWords={searchQuery.split(" ")}
                  textToHighlight={`State:  ${
                    city.cities_state
                  } | Population: ${city.cities_population.toLocaleString(
                    "en",
                    {
                      useGrouping: true,
                    }
                  )}`}
                />
              </Paragraph>
              <Paragraph>
                <Highlighter
                  searchWords={searchQuery.split(" ")}
                  textToHighlight={`Popular Sports: ${city.cities_popularsports
                    .replace(/,\s*$/, "")
                    .split(",")
                    .join(", ")}`}
                />
              </Paragraph>
            </Card>
          ))}
          {link && total > 0 ? (
            <Card
              className={styles.card}
              hoverable={true}
              onClick={() => history.push("cities?search=" + searchQuery)}
            >
              <Title level={3}>View {total} more results</Title>
            </Card>
          ) : null}
        </Row>
      )}
    </div>
  );
}
