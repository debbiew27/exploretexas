import React, { useState, useEffect } from "react";
import { BsArrowLeft } from "react-icons/bs";
import { useParams, useHistory, Link } from "react-router-dom";
import styles from "./CitiesInstance.module.css";
import { Card, Typography, Descriptions, Spin } from "antd";
import { getAPI } from "../library/Data";
const { Title } = Typography;

export default function CitiesInstance() {
  const { id } = useParams();
  const [cityData, setCityData] = useState([]);
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const data = await getAPI("https://api.welikesportz.me/cities/" + id);
        setCityData(data.data);
        setLoaded(true);
      } catch (err) {
        console.error(err);
      }
    };
    fetchData();
  }, [id]);

  const history = useHistory();
  return (
    <div>
      <div className={styles.back}>
        <BsArrowLeft
          className={styles.backButton}
          onClick={() => history.goBack()}
        ></BsArrowLeft>
      </div>
      {loaded ? (
        <div className={styles.wrapper}>
          <Title>{cityData.cities_name}</Title>
          <div className={styles.header}>
            <Card
              hoverable
              cover={
                <img
                  alt="N/A"
                  src={cityData.cities_picture}
                  className={styles.cardImage}
                />
              }
              className={styles.card}
            >
              {" "}
              {cityData.cities_name + " Wikipedia Photo"}
            </Card>
            <Card
              hoverable
              cover={
                <iframe
                  title={cityData.cities_name}
                  loading="lazy"
                  className={styles.cardImage}
                  allowfullscreen
                  src={
                    "https://www.google.com/maps/embed/v1/place?key=AIzaSyC0VtMaQz27bQ733v0uDf36yHFzVvRJVdQ&q=" +
                    cityData.cities_name
                  }
                ></iframe>
              }
              className={styles.card}
            >
              {" "}
              {cityData.cities_name + " Map"}
            </Card>
          </div>
          <Descriptions
            title="Additional Info"
            bordered
            column={{ xxl: 1, xl: 1, lg: 1, md: 1, sm: 1, xs: 1 }}
          >
            <Descriptions.Item label="State">
              {" "}
              {cityData.cities_state}{" "}
            </Descriptions.Item>
            <Descriptions.Item label="Latitude">
              {" "}
              {cityData.cities_lat}{" "}
            </Descriptions.Item>
            <Descriptions.Item label="Longitude">
              {" "}
              {cityData.cities_long}{" "}
            </Descriptions.Item>
            <Descriptions.Item label="Population">
              {" "}
              {cityData.cities_population.toLocaleString("en", {
                useGrouping: true,
              })}{" "}
            </Descriptions.Item>
            <Descriptions.Item label="Number of Sports Teams">
              {" "}
              {cityData.cities_numteams}{" "}
            </Descriptions.Item>
            <Descriptions.Item label="Number of Major League Teams">
              {" "}
              {cityData.cities_numsports}{" "}
            </Descriptions.Item>
            <Descriptions.Item label="Notable Players">
              {cityData.cities_notableplayers.map((player) => {
                return (
                  <>
                    <Link to={"/players/" + player.id}>
                      {" "}
                      {player.players_name + ","}{" "}
                    </Link>
                  </>
                );
              })}
            </Descriptions.Item>
            <Descriptions.Item label="Notable Teams">
              {cityData.cities_notableteams.map((team) => {
                return (
                  <>
                    <Link to={"/teams/" + team.id}>
                      {" "}
                      {team.teams_name + ","}{" "}
                    </Link>
                  </>
                );
              })}
            </Descriptions.Item>
            <Descriptions.Item label="Popular Sports">
              {" "}
              {cityData.cities_popularsports
                .replace(/,\s*$/, "")
                .split(",")
                .join(", ")}
            </Descriptions.Item>
          </Descriptions>
        </div>
      ) : (
        <Spin
          size="large"
          style={{
            display: "flex",
            justifyContent: "center",
          }}
        />
      )}
    </div>
  );
}
