import React, { useState, useEffect } from "react";
import styles from "./About.module.css";

import { Card, Spin } from "antd";
import Row from "react-bootstrap/Row";

import { getAPI } from "../library/Data";

const { Meta } = Card;
const teamInfo = [
  {
    name: "Bruce Moe - Phase 2 Project Leader",
    email: "brucemoe1@gmail.com",
    role: "Frontend",
    linkedin: "https://www.linkedin.com/in/bruce-moe/",
    about:
      "I'm a junior CS major at UT Austin from Dallas, Texas. In my time free I enjoy playing games, working out, watching Youtube/TV, and hanging out with friends",
    image: "/brucemoe.png",
    commits: 0,
    issues: 0,
    tests: 30,
  },
  {
    name: "Andrew Wu - Phase 1 Project Leader",
    email: "andrew.m.wu@gmail.com",
    role: "Backend",
    linkedin: "https://www.linkedin.com/in/andrewmwu/",
    about:
      "I'm a junior CS major at UT Austin. I grew up in Houston, Texas and in my free time I like to play games with friends and photograph",
    image: "/andrewwu.png",
    commits: 0,
    issues: 0,
    tests: 0,
  },
  {
    name: "Debbie Wong - Phase 3 Project Leader",
    email: "debbiewong27@gmail.com",
    role: "Backend",
    linkedin: "https://www.linkedin.com/in/debbie-wong27/",
    about:
      "I'm a fourth year CS major at UT Austin. I'm from Houston, Texas and in my free time I like to sleep and play mobile games!",
    image: "/debbiewong.png",
    commits: 0,
    issues: 0,
    tests: 0,
  },
  {
    name: "Ian Winson",
    email: "iankwinson@gmail.com",
    role: "Frontend",
    linkedin: "https://www.linkedin.com/in/ianwinson/",
    about:
      "I am a Computer Science student at the University of Texas at Austin planning to graduate in May 2022",
    image: "/ianwinson.png",
    commits: 0,
    issues: 0,
    tests: 10,
  },
  {
    name: "Victor Xia",
    role: "Backend",
    email: "victorxia@utexas.edu",
    linkedin: "http://linkedin.com/in/victor-xia-589109175",
    about:
      "I'm a third year CS major at UT Austin. I'm from Houston, Texas and spend my free time playing volleyball and video games like League of Legends and Valorant",
    image: "/victorxia.png",
    issues: 0,
    tests: 21,
  },
];

const getGitlabInfo = async () => {
  let totalCommitCount = 0,
    totalTestCount = 0,
    totalIssueCount = 0;

  teamInfo.forEach((member) => {
    totalTestCount += member.tests;
    member.issues = 0;
    member.commits = 0;
  });

  let commitList = await getAPI(
    "https://gitlab.com/api/v4/projects/29828355/repository/contributors"
  );
  commitList.data.forEach((element) => {
    const { name, email, commits } = element;
    teamInfo.forEach((member) => {
      if (member.name.includes(name) || member.email === email) {
        member.commits += commits;
      }
    });
    totalCommitCount += commits;
  });

  // const issuePaginationLength = 100;
  // let page = 1;
  // let issuePage = [];
  let issueList = await getAPI(
    "https://gitlab.com/api/v4/projects/29828355/issues"
  );
  // do {
  //   issuePage = await fetch(
  //     `https://gitlab.com/api/v4/projects/29828355/issues?per_page=${issuePaginationLength}&page=${page++}`
  //   );
  //   issuePage = await issuePage.json();
  //   issueList = [...issueList, ...issuePage];
  // } while (issuePage.length === 100);

  issueList.data.forEach((element) => {
    const { assignees } = element;
    assignees.forEach((a) => {
      const { name } = a;
      teamInfo.forEach((member) => {
        if (member.name === name) {
          member.issues += 1;
        }
      });
    });
    totalIssueCount += 1;
  });

  return {
    totalCommits: totalCommitCount,
    totalIssues: totalIssueCount,
    totalTests: totalTestCount,
    teamInfo: teamInfo,
  };
};

export default function About() {
  const [teamList, setTeamList] = useState([]);
  const [totalCommits, setTotalCommits] = useState(0);
  const [totalIssues, setTotalIssues] = useState(0);
  const [totalTests, setTotalTests] = useState(0);
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      if (teamList === undefined || teamList.length === 0) {
        const gitlabInfo = await getGitlabInfo();
        setTotalCommits(gitlabInfo.totalCommits);
        setTotalIssues(gitlabInfo.totalIssues);
        setTotalTests(gitlabInfo.totalTests);
        setTeamList(gitlabInfo.teamInfo);
        setLoaded(true);
      }
    };
    fetchData();
  }, [teamList]);

  return (
    <div className={styles.wrapper}>
      <h1 className={styles.title}>About Us</h1>

      <div>
        <p>
          WeLikeSportz allows user to connect more closely with their local
          sports teams and players or learn about sports all around the world.
          We combine data from several different API's to provide comprehensive
          information on different Teams, Players, and Cities. Our goal is to
          elegantly display data about sports culture all over the world and
          allow people to explore the history behind sports geographically.
        </p>
      </div>
      <div>
        <h2 className={styles.title}>Team Members</h2>
        <div className={styles.centered}>
          <Row xs={1} md={3} className="justify-content-md-center">
            {loaded ? (
              teamList.map((member) => (
                <div onClick={() => (window.location.href = member.linkedin)}>
                  <Card
                    className={styles.card}
                    style={{ width: 320 }}
                    hoverable={true}
                    cover={
                      <img
                        alt="example"
                        src={member.image}
                        className={styles.cardImage}
                      />
                    }
                    actions={[
                      <h6>{"Commits: " + member.commits}</h6>,
                      <h6>{"Issues: " + member.issues}</h6>,
                      <h6>{"Unit Tests: " + member.tests}</h6>,
                    ]}
                  >
                    <Meta title={member.name} description={member.about} />
                    <p>Role: {member.role}</p>
                  </Card>
                </div>
              ))
            ) : (
              <Spin size="large" />
            )}
          </Row>
        </div>
        <h2 className={styles.title}>Gitlab Statistics</h2>
        <Row xs={1} md={3} className="justify-content-md-center">
          <Card title="Total Commits">
            <h2 id="commits">{totalCommits}</h2>
          </Card>
          <Card title="Total Issues">
            <h2 id="issues">{totalIssues}</h2>
          </Card>
          <Card title="Total Tests">
            <h2 id="tests">{totalTests}</h2>
          </Card>
        </Row>
        <h2 className={styles.title}>APIs Used</h2>
        <Row xs={1} md={3} className={styles.centered}>
          <div
            onClick={() =>
              (window.location.href = "https://www.thesportsdb.com/")
            }
          >
            <Card
              className={styles.card}
              style={{ width: 320 }}
              hoverable={true}
              cover={
                <img
                  alt="example"
                  src="/apifootball.png"
                  className={styles.cardImage}
                />
              }
            >
              <Meta
                title="The Sports DB"
                description="An open, crowd-sourced database of sports artwork and metadata with a free API. Scraped using API calls for teams and players and parsed to add data"
              />
            </Card>
          </div>

          <div
            onClick={() =>
              (window.location.href =
                "https://www.back4app.com/database/back4app/usa-by-state")
            }
          >
            <Card
              className={styles.card}
              style={{ width: 320 }}
              hoverable={true}
              cover={
                <img
                  alt="example"
                  src="/rapidapi.png"
                  className={styles.cardImage}
                />
              }
            >
              <Meta
                title="Back4App"
                description="This is a comprehensive database that shows all US cities divided by state. The database is public and the connection is available via API. The use has also the option to clone the database and perform changes on it. Scraped using API calls"
              />
            </Card>
          </div>

          <div
            onClick={() =>
              (window.location.href =
                "https://developers.google.com/youtube/v3")
            }
          >
            <Card
              className={styles.card}
              style={{ width: 320 }}
              hoverable={true}
              cover={
                <img
                  alt="example"
                  src="/youtubeapi.png"
                  className={styles.cardImage}
                />
              }
            >
              <Meta
                title="YouTube Data API"
                description="Add YouTube functionality to your app. Scraped using selenium"
              />
            </Card>
          </div>
        </Row>
        <h2 className={styles.title}>Tools Used</h2>
        <Row xs={1} md={3} className="justify-content-md-center">
          <div onClick={() => (window.location.href = "https://reactjs.org/")}>
            <Card
              className={styles.card}
              style={{ width: 320 }}
              hoverable={true}
              cover={
                <img
                  alt="example"
                  src="/react.png"
                  className={styles.cardImage}
                />
              }
            >
              <Meta
                title="React"
                description="A JavaScript library for building user interfaces"
              />
            </Card>
          </div>

          <div
            onClick={() => (window.location.href = "https://aws.amazon.com/")}
          >
            <Card
              className={styles.card}
              style={{ width: 320 }}
              hoverable={true}
              cover={
                <img
                  alt="example"
                  src="/amazon.png"
                  className={styles.cardImage}
                />
              }
            >
              <Meta
                title="AWS"
                description="Using AWS amplify for web hosting"
              />
            </Card>
          </div>

          <div
            onClick={() => (window.location.href = "https://getbootstrap.com/")}
          >
            <Card
              className={styles.card}
              style={{ width: 320 }}
              hoverable={true}
              cover={
                <img
                  alt="example"
                  src="/bootstrap.png"
                  className={styles.cardImage}
                />
              }
            >
              <Meta
                title="Ant Design"
                description="An enterprise-class UI design language and React UI library with a set of high-quality React components, one of best React UI library for enterprises."
              />
            </Card>
          </div>
        </Row>
        <h2 className={styles.title}> GitLab and API Documentation</h2>
        <Row xs={1} md={3} className="justify-content-md-center">
          <div
            onClick={() =>
              (window.location.href =
                "https://gitlab.com/debbiew27/WeLikeSportz")
            }
          >
            <Card
              className={styles.card}
              style={{ width: 320 }}
              hoverable={true}
              cover={
                <img
                  alt="example"
                  src="/gitlab.png"
                  className={styles.cardImage}
                />
              }
            >
              <Meta title="GitLab" description="Continous Integration" />
            </Card>
          </div>
          <div
            onClick={() =>
              (window.location.href =
                "https://documenter.getpostman.com/view/16128389/UUy4ckQD")
            }
          >
            <Card
              className={styles.card}
              style={{ width: 320 }}
              hoverable={true}
              cover={
                <img
                  alt="example"
                  src="/postman.png"
                  className={styles.cardImage}
                />
              }
            >
              <Meta title="Postman" description="API documentation" />
            </Card>
          </div>
        </Row>
      </div>
    </div>
  );
}
