import React, { useEffect, useState } from "react";
import { getAPI } from "../../library/Data";
import { Spin, Typography } from "antd";
import { useHistory } from "react-router-dom";
import {
  Bar,
  BarChart,
  CartesianGrid,
  Legend,
  XAxis,
  YAxis,
  Tooltip,
} from "recharts";

const { Title } = Typography;
const leagues = ["NFL", "NBA", "AHL", "American Major League Soccer"];
const leaguesDisplay = ["NFL", "NBA", "AHL", "MLS"];

export default function TeamVis() {
  const history = useHistory();
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const getData = async () => {
      setLoading(true);
      try {
        const tempData = [];
        for (let i = 0; i < leagues.length; i++) {
          let filterQuery = {};
          let filterList = [];
          filterList.push({
            name: "teams_league",
            op: "eq",
            val: leagues[i],
          });
          filterQuery["filters"] = filterList;
          const query = JSON.stringify(filterQuery);
          const teamsData = await getAPI(
            "https://api.welikesportz.me/teams?q=" + query
          );
          const leagueStats = {
            name: leaguesDisplay[i],
            Teams: teamsData.data.num_results,
          };

          tempData.push(leagueStats);
        }
        setData(tempData);
        setLoading(false);
      } catch (err) {
        console.error(err);
      }
    };
    getData();
  }, []);

  return (
    <>
      <Title
        style={{ marginTop: "30px", textDecoration: "underline" }}
        level={3}
      >
        Teams per League
      </Title>
      {loading ? (
        <Spin></Spin>
      ) : (
        <BarChart
          onClick={({ activePayload }) => {
            history.push(`teams?league=${activePayload[0].payload.name}`);
          }}
          width={800}
          height={400}
          barSize={20}
          data={data}
          margin={{
            top: 5,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Legend />
          <Bar dataKey="Teams" fill="#8884d8" />
        </BarChart>
      )}
    </>
  );
}
