import React, { useEffect, useState } from "react";
import { getAPI } from "../../library/Data";
import { Spin, Typography } from "antd";
import {
  ScatterChart,
  Scatter,
  XAxis,
  YAxis,
  ZAxis,
  CartesianGrid,
  Tooltip,
} from "recharts";
import { useHistory } from "react-router-dom";

const { Title } = Typography;

function findHeight(height) {
  if (height === null) {
    return -1;
  }
  const values = height.match(/\d+/g);
  if (values === null || values[0] > 7 || values[0] < 4) {
    return -1;
  }
  const feet = parseFloat(values[0]);
  const inches = values[1] !== null ? parseFloat(values[1] / 12) : 0;
  return feet + inches;
}

function findWeight(weight) {
  if (weight === null) {
    return -1;
  }
  const values = weight.match(/\d+/g);
  if (values === null) {
    return -1;
  }
  const other = values[1] !== null ? parseInt(values[1]) : 0;
  return Math.max(other, parseInt(values[0]));
}

export default function PlayersVis() {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [playerID] = useState(new Map());
  const history = useHistory();

  useEffect(() => {
    const getData = async () => {
      setLoading(true);
      let tempData = [];
      const playerData = await getAPI(
        `https://api.welikesportz.me/players?results_per_page=100`
      );
      const data = playerData.data.objects;
      let playerStats = {};
      for (let i = 0; i < data.length; i++) {
        playerID.set(data[i].players_name, data[i].id);
        const playerHeight = findHeight(data[i].players_height);
        if (playerHeight === -1) {
          continue;
        }
        const playerWeight = findWeight(data[i].players_weight);
        if (playerWeight === -1) {
          continue;
        }
        playerStats[data[i].players_name] = [playerHeight, playerWeight];
      }
      for (var key in playerStats) {
        const curr = {
          name: key,
          height: playerStats[key][0],
          weight: playerStats[key][1],
        };
        tempData.push(curr);
      }
      setData(tempData);
      setLoading(false);
    };
    getData();
  }, [playerID]);

  return (
    <>
      <Title
        style={{ marginTop: "30px", textDecoration: "underline" }}
        level={3}
      >
        Players by Weight/Height
      </Title>
      {loading ? (
        <Spin> </Spin>
      ) : (
        <ScatterChart
          width={800}
          height={400}
          margin={{
            top: 20,
            right: 20,
            bottom: 20,
            left: 20,
          }}
        >
          <CartesianGrid />
          <XAxis type="number" dataKey="height" name="height" unit="ft" />
          <YAxis type="number" dataKey="weight" name="weight" unit="lbs" />
          <ZAxis dataKey="name" range={[64, 144]} name="name" />
          <Tooltip cursor={{ strokeDasharray: "3 3" }} />
          <Scatter
            onClick={(e) => {
              history.push(`players/${playerID.get(e.name)}`);
            }}
            name="A school"
            data={data}
            fill="#8884d8"
          />
        </ScatterChart>
      )}
    </>
  );
}
