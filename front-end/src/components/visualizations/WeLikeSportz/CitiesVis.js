import React, { useEffect, useState } from "react";
import { getAPI } from "../../library/Data";
import { Spin, Typography } from "antd";
import BubbleChart from "@weknow/react-bubble-chart-d3";
import { useHistory } from "react-router-dom";

const { Title } = Typography;

export default function CitiesVis() {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [cityID] = useState(new Map());
  const history = useHistory();

  useEffect(() => {
    const parseData = (data) => {
      const output = data.map((city) => {
        return { label: city.cities_name, value: city.cities_population };
      });
      return output.slice(0, 20);
    };
    const getData = async () => {
      setLoading(true);
      const cityData = await getAPI(
        `https://api.welikesportz.me/cities?q={"order_by":[{"field":"cities_population","direction":"desc"}]}`
      );
      const data = cityData.data.objects;
      data.forEach((city) => {
        cityID.set(city.cities_name, city.id);
      });
      const parsedData = parseData(data);
      setData(parsedData);
      setLoading(false);
    };
    getData();
  }, [cityID]);

  return (
    <>
      <Title
        style={{
          marginTop: "30px",
          textDecoration: "underline",
          marginBottom: "30px",
        }}
        level={3}
      >
        Most Populous Cities
      </Title>
      {loading ? (
        <Spin> </Spin>
      ) : (
        <div>
          <BubbleChart
            graph={{
              zoom: 1,
            }}
            showLegend={false}
            width={500}
            height={500}
            valueFont={{
              family: "Arial",
              size: 12,
              color: "#fff",
              weight: "bold",
            }}
            labelFont={{
              family: "Arial",
              size: 10,
              color: "#fff",
              weight: "bold",
            }}
            data={data}
            bubbleClickFun={(val) => {
              history.push(`/cities/${cityID.get(val)}`);
            }}
          />
        </div>
      )}
    </>
  );
}
