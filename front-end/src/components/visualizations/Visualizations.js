import React from "react";
import styles from "./Visualizations.module.css";
import Row from "react-bootstrap/Row";
import ProviderTeamVis from "./Provider/TeamVis";
import ProviderPlayerVis from "./Provider/PlayerVis";
import ProviderMatchVis from "./Provider/MatchVis";
import TeamVis from "./WeLikeSportz/TeamsVis";
import CitiesVis from "./WeLikeSportz/CitiesVis";
import PlayersVis from "./WeLikeSportz/PlayersVis";

export default function Visualizations() {
  return (
    <div className={styles.wrapper}>
      <div className={styles.header}>
        <h1 className={styles.title}>Visualizations</h1>
      </div>
      <div className={styles.section}>
        <Row
          xs={1}
          md={1}
          className="justify-content-md-center row g-2"
          style={{ marginBottom: "30px" }}
        >
          <h2 style={{ fontStyle: "italic" }}>Our Visualizations</h2>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <TeamVis />
            <CitiesVis />
            <PlayersVis />
          </div>
        </Row>
      </div>
      <div className={styles.section}>
        <Row
          xs={1}
          md={1}
          className="justify-content-md-center row g-2"
          style={{ marginBottom: "30px" }}
        >
          <h2 style={{ fontStyle: "italic" }}>
            <a
              target="_blank"
              rel="noreferrer"
              href={"https://www.football-stats.me/"}
            >
              Provider Visualizations
            </a>
          </h2>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <ProviderPlayerVis />
            <ProviderTeamVis />
            <ProviderMatchVis />
          </div>
        </Row>
      </div>
    </div>
  );
}
