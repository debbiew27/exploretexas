import React, { useEffect, useState } from "react";
import { getAPI } from "../../library/Data";
import { Spin, Typography } from "antd";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
} from "recharts";

const { Title } = Typography;

export default function MatchVis() {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const getData = async () => {
      setLoading(true);
      const matchData = await getAPI(
        "https://api.football-stats.me/matches?perPage=203"
      );
      const data = matchData.data.matches;
      let leagueCount = {};
      for (let i = 0; i < data.length; i++) {
        const league = data[i].match_league;
        if (!(league in leagueCount)) {
          leagueCount[league] = 0;
        }
        leagueCount[league] = leagueCount[league] + 1;
      }
      let tempData = [];
      for (var key in leagueCount) {
        const curr = {
          name: key,
          Matches: leagueCount[key],
        };
        tempData.push(curr);
      }
      setData(tempData);
      setLoading(false);
    };
    getData();
  }, []);

  return (
    <>
      <Title
        style={{ marginTop: "30px", textDecoration: "underline" }}
        level={3}
      >
        Matches per League
      </Title>
      {loading ? (
        <Spin> </Spin>
      ) : (
        <BarChart
          width={800}
          height={400}
          data={data}
          margin={{
            top: 5,
            right: 30,
            left: 20,
            bottom: 5,
          }}
          barSize={20}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Legend />
          <Bar dataKey="Matches" fill="#8884d8" />
        </BarChart>
      )}
    </>
  );
}
