import { cities, teams } from "../library/Data";

const jerseyNums = [];
for (let i = 1; i <= 99; i++) {
  jerseyNums[i] = "" + i;
}

export const attributes = [
  {
    name: "Name",
    multiple: false,
    fields: ["A-Z", "Z-A"],
    desc: "Sort by Name",
  },
  {
    name: "City",
    multiple: true,
    fields: cities,
    desc: "Filter by City",
  },
  {
    name: "Team",
    multiple: true,
    fields: teams,
    desc: "Filter by Team",
  },
  {
    name: "Sport",
    multiple: true,
    fields: ["Basketball", "Soccer", "American Football", "Ice Hockey"],
    desc: "Filter by Sport",
  },
  {
    name: "Jersey Number",
    multiple: true,
    fields: jerseyNums,
    desc: "Filter by Jersey Number",
  },
];
