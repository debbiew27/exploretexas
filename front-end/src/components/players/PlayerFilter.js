export function filterInfo(filters) {
  let filterQuery = {};
  if (filters.name === "A-Z") {
    filterQuery["order_by"] = [{ field: "players_name", direction: "asc" }];
  } else if (filters.name === "Z-A") {
    filterQuery["order_by"] = [{ field: "players_name", direction: "desc" }];
  }
  let filterList = [];
  if (filters.sport.length > 0) {
    filterList.push({
      name: "players_sport",
      op: "in",
      val: filters.sport,
    });
  }
  if (filters.jerseynumber.length > 0) {
    filterList.push({
      name: "players_jerseynum",
      op: "in",
      val: filters.jerseynumber,
    });
  }
  if (filters.team.length > 0) {
    filterList.push({
      name: "players_team",
      op: "in",
      val: filters.team,
    });
  }
  if (filters.city.length > 0) {
    filterList.push({
      name: "players_city",
      op: "in",
      val: filters.city,
    });
  }
  filterQuery["filters"] = filterList;
  return JSON.stringify(filterQuery);
}
