import React, { useState, useEffect } from "react";
import { BsArrowLeft } from "react-icons/bs";
import { useParams, useHistory, Link } from "react-router-dom";
import styles from "./PlayerInstance.module.css";
import { Card, Typography, Descriptions, Spin } from "antd";
import { getAPI } from "../library/Data";
const { Title } = Typography;

export default function PlayerInstance() {
  const { id } = useParams();
  const [playerData, setPlayerData] = useState([]);
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const data = await getAPI("https://api.welikesportz.me/players/" + id);
        setPlayerData(data.data);
        setLoaded(true);
      } catch (err) {
        console.error(err);
      }
    };
    fetchData();
  }, [id]);

  const history = useHistory();
  return (
    <div>
      <div className={styles.back}>
        <BsArrowLeft
          className={styles.backButton}
          onClick={() => history.goBack()}
        ></BsArrowLeft>
      </div>
      {loaded ? (
        <div className={styles.wrapper}>
          <Title>{playerData.players_name}</Title>
          <div className={styles.header}>
            <Card
              hoverable
              cover={
                <img
                  alt="N/A"
                  src={playerData.players_picture}
                  className={styles.cardImage}
                />
              }
              className={styles.card}
            >
              {" "}
              {"Photo of " + playerData.players_name}
            </Card>
            <Card
              hoverable
              cover={
                <iframe
                  className={styles.cardImage}
                  src={`https://www.youtube.com/embed/${playerData.players_youtubeid}`}
                  frameBorder="0"
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                  allowFullScreen
                  title="Embedded youtube"
                />
              }
              className={styles.card}
            >
              {" "}
              {"Video of " + playerData.players_name}
            </Card>
          </div>
          <Descriptions
            title="Additional Info"
            bordered
            column={{ xxl: 1, xl: 1, lg: 1, md: 1, sm: 1, xs: 1 }}
          >
            <Descriptions.Item label={"Description"}>
              {" "}
              {playerData.players_description}{" "}
            </Descriptions.Item>
            <Descriptions.Item label={"Date of Birth"}>
              {" "}
              {playerData.players_birthday}{" "}
            </Descriptions.Item>
            <Descriptions.Item label={"Height"}>
              {" "}
              {playerData.players_height}
            </Descriptions.Item>
            <Descriptions.Item label={"Weight"}>
              {" "}
              {playerData.players_weight}
            </Descriptions.Item>
            <Descriptions.Item label={"State"}>
              {" "}
              {playerData.players_state}{" "}
            </Descriptions.Item>
            <Descriptions.Item label={"City"}>
              {" "}
              <Link to={"/cities/" + playerData.players_cityid}>
                {" "}
                {playerData.players_city}
              </Link>{" "}
            </Descriptions.Item>
            <Descriptions.Item label={"Sport"}>
              {" "}
              {playerData.players_sport}{" "}
            </Descriptions.Item>
            <Descriptions.Item label={"Team"}>
              {" "}
              <Link to={"/teams/" + playerData.players_teamid}>
                {" "}
                {playerData.players_team}
              </Link>
            </Descriptions.Item>
            <Descriptions.Item label={"Jersey Number"}>
              {" "}
              {playerData.players_jerseynum === null ||
              playerData.players_jerseynum === ""
                ? "N/A"
                : playerData.players_jerseynum}{" "}
            </Descriptions.Item>
            <Descriptions.Item label={"Birthplace"}>
              {" "}
              {playerData.players_origin}{" "}
            </Descriptions.Item>
            <Descriptions.Item label={"Facebook Page"}>
              {" "}
              {playerData.players_facebook === null ||
              playerData.players_facebook === "" ? (
                "N/A"
              ) : (
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href={"https://" + playerData.players_facebook}
                >
                  {playerData.players_facebook}{" "}
                </a>
              )}
            </Descriptions.Item>
          </Descriptions>
        </div>
      ) : (
        <Spin
          size="large"
          style={{
            display: "flex",
            justifyContent: "center",
          }}
        />
      )}
    </div>
  );
}
