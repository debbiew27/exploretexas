import React, { useState, useEffect } from "react";
import { BsArrowLeft } from "react-icons/bs";
import { useParams, useHistory, Link } from "react-router-dom";
import styles from "./TeamsInstance.module.css";
import { Card, Typography, Descriptions, Spin } from "antd";
import { getAPI } from "../library/Data";
const { Title } = Typography;

export default function TeamsInstance() {
  const { id } = useParams();
  const [teamData, setTeamData] = useState([]);
  const [loaded, setLoaded] = useState(false);
  useEffect(() => {
    window.FB.XFBML.parse();
  });

  useEffect(() => {
    const fetchData = async () => {
      try {
        const data = await getAPI("https://api.welikesportz.me/teams/" + id);
        setTeamData(data.data);
        setLoaded(true);
      } catch (err) {
        console.error(err);
      }
    };
    fetchData();
  }, [id]);
  const history = useHistory();
  return (
    <div>
      <div className={styles.back}>
        <BsArrowLeft
          className={styles.backButton}
          onClick={() => history.goBack()}
        ></BsArrowLeft>
      </div>
      {loaded ? (
        <div className={styles.wrapper}>
          <Title>{teamData.teams_name}</Title>
          <div className={styles.header}>
            <Card
              hoverable
              cover={
                <img
                  alt="N/A"
                  src={teamData.teams_logo}
                  className={styles.cardImage}
                />
              }
              className={styles.card}
            >
              {" "}
              {teamData.teams_name + " Logo"}
            </Card>
            <Card
              hoverable
              cover={
                <div
                  className="fb-page"
                  height={window.innerWidth <= 763 ? "200" : "400"}
                  width={window.innerWidth <= 763 ? "200" : "400"}
                  data-href={"http://" + teamData.teams_facebook}
                  data-tabs="timeline"
                  data-small-header="false"
                  data-adapt-container-width="false"
                  data-hide-cover="false"
                  data-show-facepile="true"
                >
                  <blockquote
                    cite={teamData.teams_facebook}
                    class="fb-xfbml-parse-ignore"
                  >
                    <a
                      rel="noreferrer"
                      target="_blank"
                      href={teamData.teams_facebook}
                    >
                      Facebook
                    </a>
                  </blockquote>
                </div>
              }
              className={styles.card}
            >
              {" "}
              {teamData.teams_name + " Facebook Page"}
            </Card>
          </div>
          <Descriptions
            title="Additional Info"
            bordered
            column={{ xxl: 1, xl: 1, lg: 1, md: 1, sm: 1, xs: 1 }}
          >
            <Descriptions.Item label={"Sport"}>
              {" "}
              {teamData.teams_sport}{" "}
            </Descriptions.Item>
            <Descriptions.Item label="Notable Players">
              {teamData.teams_notableplayers.map((player) => {
                return (
                  <>
                    <Link to={"/players/" + player.id}>
                      {" "}
                      {player.players_name + ","}{" "}
                    </Link>
                  </>
                );
              })}
            </Descriptions.Item>
            <Descriptions.Item label={"State"}>
              {" "}
              {teamData.teams_state}
            </Descriptions.Item>
            <Descriptions.Item label={"City"}>
              {" "}
              <Link to={"/cities/" + teamData.teams_cityid}>
                {" "}
                {teamData.teams_city}
              </Link>
            </Descriptions.Item>
            <Descriptions.Item label={"League"}>
              {" "}
              {teamData.teams_league}
            </Descriptions.Item>
            <Descriptions.Item label={"Stadium"}>
              {" "}
              {teamData.teams_stadium}
            </Descriptions.Item>
            <Descriptions.Item label={"Stadium Capacity"}>
              {" "}
              {teamData.teams_stadiumcapacity.toLocaleString("en", {
                useGrouping: true,
              })}{" "}
            </Descriptions.Item>
            <Descriptions.Item label={"Year Formed"}>
              {" "}
              {teamData.teams_yearformed}{" "}
            </Descriptions.Item>
          </Descriptions>
          <div>
            <Card
              hoverable
              cover={
                <iframe
                  title={teamData.teams_stadium}
                  loading="lazy"
                  className={styles.cardImage}
                  allowfullscreen
                  src={
                    "https://www.google.com/maps/embed/v1/place?key=AIzaSyC0VtMaQz27bQ733v0uDf36yHFzVvRJVdQ&q=" +
                    teamData.teams_stadium +
                    teamData.teams_state
                  }
                ></iframe>
              }
              className={styles.card}
            >
              {" "}
              {teamData.teams_stadium + " on a map"}
            </Card>
          </div>
        </div>
      ) : (
        <Spin
          size="large"
          style={{
            display: "flex",
            justifyContent: "center",
          }}
        />
      )}
    </div>
  );
}
