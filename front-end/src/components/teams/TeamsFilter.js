export function filterInfo(filters) {
  let filterQuery = {};
  if (filters.name === "A-Z") {
    filterQuery["order_by"] = [{ field: "teams_name", direction: "asc" }];
  } else if (filters.name === "Z-A") {
    filterQuery["order_by"] = [{ field: "teams_name", direction: "desc" }];
  }
  let filterList = [];
  if (filters.league.length > 0) {
    filterList.push({
      name: "teams_league",
      op: "in",
      val: filters.league,
    });
  }
  if (filters.city.length > 0) {
    filterList.push({
      name: "teams_city",
      op: "in",
      val: filters.city,
    });
  }
  if (filters.sport.length > 0) {
    filterList.push({
      name: "teams_sport",
      op: "in",
      val: filters.sport,
    });
  }
  if (filters.stadium.length > 0) {
    filterList.push({
      name: "teams_stadium",
      op: "in",
      val: filters.stadium,
    });
  }
  filterQuery["filters"] = filterList;
  return JSON.stringify(filterQuery);
}
