import React, { useState, useEffect } from "react";
import { Select, Pagination, Spin, Input } from "antd";
import styles from "./Teams.module.css";
import Row from "react-bootstrap/Row";
import { attributes } from "./TeamInfo";
import { filterInfo } from "./TeamsFilter";
import { getAPI } from "../library/Data";
import TeamsGrid from "./TeamsGrid";
import TeamsSearch from "./TeamsSearch";
import {
  useQueryParams,
  StringParam,
  NumberParam,
  ArrayParam,
  withDefault,
} from "use-query-params";

const { Search } = Input;
const { Option } = Select;

export default function Teams() {
  const [data, setData] = useState([]);
  const [loaded, setLoaded] = useState(false);
  const [total, setTotal] = useState(0);
  const [searchView, setSearchView] = useState(false);

  const [params, setParams] = useQueryParams({
    search: withDefault(StringParam, ""),
    page: withDefault(NumberParam, 1),
    perPage: withDefault(NumberParam, 9),
    name: StringParam,
    league: withDefault(ArrayParam, []),
    city: withDefault(ArrayParam, []),
    sport: withDefault(ArrayParam, []),
    stadium: withDefault(ArrayParam, []),
  });

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoaded(false);
        const fetchData = await getAPI(
          "https://api.welikesportz.me/teams?results_per_page=" +
            params.perPage +
            "&page=" +
            params.page +
            "&q=" +
            filterInfo(params)
        );
        setData(fetchData.data.objects);
        setTotal(fetchData.data.num_results);
        setLoaded(true);
      } catch (err) {
        console.error(err);
      }
    };
    const fetchSearchData = async () => {
      try {
        setLoaded(false);
        const fetchData = await getAPI(
          "https://api.welikesportz.me/search_teams?results_per_page=" +
            params.perPage +
            "&page=" +
            params.page +
            "&q=" +
            params.search +
            "&" +
            filterInfo(params)
        );
        setData(fetchData.data.objects);
        setTotal(fetchData.data.num_results);
        setLoaded(true);
      } catch (err) {
        console.error(err);
      }
    };
    if (params.search !== "") {
      fetchSearchData();
      setSearchView(true);
    } else {
      fetchData();
      setSearchView(false);
    }
  }, [params]);

  return (
    <div className={styles.wrapper}>
      <div className={styles.header}>
        <h1>Teams</h1>
        <div className={styles.search}>
          <Search
            placeholder="Search Teams"
            enterButton
            defaultValue={params.search}
            size="large"
            onSearch={(value) =>
              setParams((params) => ({ ...params, search: value, page: 1 }))
            }
          />
        </div>
        <Row
          xs={1}
          md={5}
          className="justify-content-md-center row g-2"
          style={{ marginBottom: "30px" }}
        >
          {attributes.map((attribute) => (
            <Select
              mode={attribute.multiple === true ? "multiple" : ""}
              allowClear
              value={params[attribute.name.toLowerCase().replace(/ +/g, "")]}
              maxTagCount={0}
              showArrow="true"
              placeholder={attribute.desc}
              onDeselect={(option) => {
                var newFilter = {
                  ...params,
                };
                const options =
                  newFilter[attribute.name.toLowerCase().replace(/ +/g, "")];
                const index = options.indexOf(option);
                options.splice(index, 1);
                setParams(newFilter);
              }}
              onChange={(event) => {
                const newFilter = {
                  ...params,
                };
                newFilter[attribute.name.toLowerCase().replace(/ +/g, "")] =
                  event;
                setParams(newFilter);
              }}
            >
              {attribute.fields.map((field) => (
                <Option disabled={attribute.disabled} value={field}>
                  {field}
                </Option>
              ))}
            </Select>
          ))}
        </Row>
        <Row xs={1} md={1} className="justify-content-md-center">
          {loaded ? (
            !searchView ? (
              <TeamsGrid data={data} />
            ) : (
              <TeamsSearch
                data={data}
                searchQuery={params.search}
                link={false}
                total={total}
              />
            )
          ) : (
            <Spin
              size="large"
              style={{ marginTop: "20px", marginBottom: "20px" }}
            />
          )}
          <Pagination
            style={{ marginTop: "30px" }}
            total={total}
            onChange={(pageNumber, pageSize) => {
              setParams((params) => ({
                ...params,
                page: pageNumber,
                perPage: pageSize,
              }));
            }}
            showTotal={(total) => `Total ${total} items`}
            current={params.page}
            pageSize={params.perPage}
            pageSizeOptions={[9, 18, 27]}
          />
        </Row>
      </div>
    </div>
  );
}
