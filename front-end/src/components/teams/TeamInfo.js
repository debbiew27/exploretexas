import { cities, stadiums } from "../library/Data";

export const attributes = [
  {
    name: "Name",
    multiple: false,
    fields: ["A-Z", "Z-A"],
    desc: "Sort by Name",
  },
  {
    name: "League",
    multiple: true,
    fields: ["NBA", "American Major League Soccer", "NFL", "AHL"],
    desc: "Filter by League",
  },
  {
    name: "City",
    multiple: true,
    fields: cities,
    desc: "Filter by City",
  },
  {
    name: "Sport",
    multiple: true,
    fields: ["Basketball", "Soccer", "American Football", "Ice Hockey"],
    desc: "Filter by Sport",
  },
  {
    name: "Stadium",
    multiple: true,
    fields: stadiums,
    desc: "Filter by Stadium",
  },
];
