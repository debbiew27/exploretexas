# WeLikeSportz Backend

To run, cd into backend and use "make docker-dev-build" for development and "make docker-build" for production.
Then, "make docker-dev-run" or "make docker-run" to run the docker image and launch the flask app for the API locally.
Use "make test" for unit testing the backend.
