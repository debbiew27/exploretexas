from models import WeLikeSportzCities, WeLikeSportzTeams, WeLikeSportzPlayers, db
import json
import urllib
import requests
from googleapiclient.discovery import build
from dotenv import load_dotenv
import os
from pathlib import Path
from re import search
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from serpapi import GoogleSearch

load_dotenv(dotenv_path=Path("./dontlook.env"))
YT_API_KEY = os.getenv("YT_API_KEY")
X_APP_KEY = os.getenv("X_APP_KEY")
X_REST_KEY = os.getenv("X_REST_KEY")

youtube = build("youtube", "v3", developerKey=YT_API_KEY)
# Key City ID String, Value list of Strings
city_to_teams = dict()
city_to_players = dict()
player_name_to_city_name = dict()
team_name_to_city_name = dict()
city_name_to_city_id = dict()
team_name_to_teamid = dict()
# Key City ID String, Value list of sports
city_to_sports = dict()
seen_players = set()
seen_teams = set()


def hard_reset_db():
    """
    USE WITH CAUTION!
    Deletes db tables and creates new ones!
    """
    db.session.remove()
    db.drop_all()
    db.create_all()


def getYoutubeLink(player_name):
    """
    Parses youtube ID from youtube API.
    """
    try:
        request = youtube.search().list(
            part="snippet",
            order="relevance",
            q=player_name,
            type="video",
            videoDefinition="high",
        )
        response = request.execute()
        videoID = response["items"][0]["id"]["videoId"]
        return videoID
    except:
        return "NaN"


def get_video_results(name):
    """
    Gets youtube ID through web scraping via selenium.
    """
    driver = webdriver.Chrome(ChromeDriverManager().install())
    split_name = name.split(" ")
    search_name = split_name[0]
    if len(split_name) == 2:
        search_name += "+" + split_name[1] + "+highlights"
    driver.get("https://www.youtube.com/results?search_query=" + search_name)

    for result in driver.find_elements_by_css_selector(
        ".text-wrapper.style-scope.ytd-video-renderer"
    ):
        link = result.find_element_by_css_selector(
            ".title-and-badge.style-scope.ytd-video-renderer a"
        ).get_attribute("href")
        driver.quit()
        return link.split("?v=")[1]


def getCityPicture(city, state):
    """
    Gets picture of city from wikipedia.
    """
    try:
        S = requests.Session()
        URL = "https://en.wikipedia.org/w/api.php"
        SEARCHPAGE = city + state

        PARAMS = {
            "action": "query",
            "format": "json",
            "list": "search",
            "srsearch": SEARCHPAGE,
        }

        R = S.get(url=URL, params=PARAMS)
        DATA = R.json()

        pageid = DATA["query"]["search"][0]["pageid"]
        newurl = (
            URL
            + "/?action=query&prop=pageimages&format=json&piprop=original&pageids="
            + str(pageid)
        )
        R = S.get(url=newurl)
        DATA = R.json()

        return DATA["query"]["pages"][str(pageid)]["original"]["source"]
    except:
        return "NaN"


def getPlayers(team_name, db, city, state, team_id):
    """
    Gets players from sportsDB API and adds into our DB.
    """
    string_of_players = ""
    player_list = []
    url = (
        "https://www.thesportsdb.com/api/v1/json/40130162/lookup_all_players.php?id="
        + str(team_id)
    )
    r = urllib.request.urlopen(url)
    data = json.loads(r.read())
    for player in data["player"]:
        name = player["strPlayer"]
        if name in seen_players:
            continue
        else:
            seen_players.add(name)
        player_name_to_city_name[name] = city
        sport = player["strSport"]
        birthday = player["dateBorn"]
        height = player["strHeight"]
        weight = player["strWeight"]
        team_id = player["idTeam"]
        origin = player["strBirthLocation"]
        jersey = player["strNumber"]
        description = player["strDescriptionEN"]
        picture = player["strThumb"]
        facebook = player["strFacebook"]
        new_player = WeLikeSportzPlayers(
            name,
            state,
            city,
            sport,
            birthday,
            height,
            weight,
            team_name,
            team_id,
            origin,
            jersey,
            description,
            picture,
            facebook,
            0,
            "NaN",
        )
        player_list.append(new_player)
        # print("Adding player " + name)
        db.session.add_all(player_list)
        # print("Finished adding player")
        db.session.commit()
        string_of_players += str(new_player.id) + ","
        # print("Finished commit")
    return string_of_players


if __name__ == "main":

    hard_reset_db()

    sport_dict = {
        "NBA": "Basketball",
        "NFL": "American Football",
        "AHL": "Hockey",
        "American Major League Soccer": "Soccer",
    }
    league_url_list = ["NBA", "NFL", "AHL", "American%20Major%20League%20Soccer"]
    black_list = {"Chicago Fire", "DC United", "Los Angeles FC", "New York City"}
    # Adds teams and fills out information from sportsDB.
    for i in range(len(league_url_list)):
        league = league_url_list[i]
        request_url = (
            "https://www.thesportsdb.com/api/v1/json/1/search_all_teams.php?l=" + league
        )
        r = urllib.request.urlopen(request_url)
        data = json.loads(r.read())
        if i == 3:
            league = "American Major League Soccer"

        for items in data["teams"]:
            team_list = []
            team_name = items["strTeam"]
            if team_name in black_list:
                continue
            if team_name in seen_teams:
                continue
            else:
                seen_teams.add(team_name)
            year_formed = items["intFormedYear"]
            stadium = items["strStadium"]
            stadium_capacity = items["intStadiumCapacity"]
            stadium_location = items["strStadiumLocation"]
            city = ""
            state = ""
            if not stadium_location is None:
                var = stadium_location.split(", ")
                city = var[0]
                state = var[1]

                if city not in city_to_sports:
                    city_to_sports[city] = [sport_dict[league]]
                else:
                    if sport_dict[league] not in city_to_sports[city]:
                        city_to_sports[city].append(sport_dict[league])

            description = items["strDescriptionEN"]
            league = items["strLeague"]
            sport = items["strSport"]
            badge = items["strTeamBadge"]
            banner = items["strTeamBanner"]
            logo = items["strTeamLogo"]
            facebook = items["strFacebook"]
            # Get and process players on team
            string_of_players = getPlayers(team_name, db, city, state, items["idTeam"])
            if city not in city_to_players:
                city_to_players[city] = string_of_players
            else:
                string_players = city_to_players[city]
                string_players += string_of_players
                city_to_players[city] = string_players

            team_name_to_city_name[team_name] = city
            team = WeLikeSportzTeams(
                team_name,
                state,
                city,
                league,
                stadium_capacity,
                sport,
                string_of_players,
                stadium,
                year_formed,
                badge,
                banner,
                logo,
                facebook,
                0,
                "NaN",
            )
            team_list.append(team)

            db.session.add_all(team_list)
            db.session.commit()
            team_name_to_teamid[team_name] = team.id

            if city is not None:
                if city not in city_to_teams:
                    city_to_teams[city] = [team.id]
                else:
                    city_to_teams[city].append(team.id)

    print("Done with teams")
    state_list = [
        "AL",
        "AK",
        "AZ",
        "AR",
        "CA",
        "CO",
        "CT",
        "DE",
        "FL",
        "GA",
        "HI",
        "ID",
        "IL",
        "IN",
        "IA",
        "KS",
        "KY",
        "LA",
        "ME",
        "MD",
        "MA",
        "MI",
        "MN",
        "MS",
        "MO",
        "MT",
        "NE",
        "NV",
        "NH",
        "NJ",
        "NM",
        "NY",
        "NC",
        "ND",
        "OH",
        "OK",
        "OR",
        "PA",
        "RI",
        "SC",
        "SD",
        "TN",
        "TX",
        "UT",
        "VT",
        "VA",
        "WA",
        "WV",
        "WI",
        "WY",
    ]

    headers = {
        "X-Parse-Application-Id": X_APP_KEY,  # This is your app's application id
        "X-Parse-REST-API-Key": X_REST_KEY,  # This is your app's REST API key
    }

    city_name_to_city_data = dict()
    # Adds cities and fills out information via cities API.
    for state in state_list:
        cities_list = []
        print("Processing state " + state)
        url = (
            "https://parseapi.back4app.com/classes/Usabystate_"
            + state
            + "?count=1&limit=10000&order=name"
        )
        data = json.loads(
            requests.get(url, headers=headers).content.decode("utf-8")
        )  # Here you have the data
        for item in data["results"]:
            if item["name"] in city_to_players:
                latitude = item["location"]["latitude"]
                longitude = item["location"]["longitude"]
                name = item["name"]
                population = item["population"]
                picture = ""
                if name == "Houston":
                    picture = "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8a/Buildings-city-houston-skyline-1870617.jpg/2880px-Buildings-city-houston-skyline-1870617.jpg"
                else:
                    picture = getCityPicture(item["name"], state)
                if name not in city_name_to_city_data:
                    city_name_to_city_data[name] = {
                        "state": state,
                        "long": longitude,
                        "lat": latitude,
                        "population": population,
                        "picture": picture,
                    }
                else:
                    # already in the list. Remove the smaller population.
                    print("Found duplicate city ", name, " Current state: ", state)
                    if city_name_to_city_data[name]["population"] < population:
                        print("Replacing with this city and state")
                        city_name_to_city_data[name] = {
                            "state": state,
                            "long": longitude,
                            "lat": latitude,
                            "population": population,
                            "picture": picture,
                        }

    for city in city_name_to_city_data:
        city_info = city_name_to_city_data[city]
        cities_list = []
        new_city = WeLikeSportzCities(
            city,
            city_info["state"],
            city_info["lat"],
            city_info["long"],
            city_info["population"],
            0,
            0,
            "NaN",
            "NaN",
            "NaN",
            city_info["picture"],
            "NaN",
        )
        cities_list.append(new_city)
        db.session.add_all(cities_list)
        db.session.commit()

    print("Added all cities to db")

    for city in city_to_players:
        # perform lookup of this city in the city db, get row
        # populate the row with the remaining fields (notable players, notable teams, num sports, popular sports)
        user = WeLikeSportzCities.query.filter_by(cities_name=city).first()
        if user is not None:

            user.cities_numteams = len(city_to_players[city].split(","))
            user.cities_numsports = len(city_to_sports[city])
            user.cities_notableplayers = city_to_players[city]
            string_team = ""
            for teamid in city_to_teams[city]:
                string_team += str(teamid) + ","
            user.cities_notableteams = string_team
            string_sports = ""
            for sport in city_to_sports[city]:
                string_sports += sport + ","
            user.cities_popularsports = string_sports
            db.session.commit()
        else:
            print(city)

    db.session.query(WeLikeSportzCities).filter(
        WeLikeSportzCities.cities_numteams == 0
    ).delete()
    db.session.commit()

    blacklist_cities = ["Laval", "Winnipeg", "Citrus Bowl Place"]
    # Populates more information in players and teams.
    for player_name in player_name_to_city_name:
        user = WeLikeSportzPlayers.query.filter_by(players_name=player_name).first()
        if user is not None:
            city_name = player_name_to_city_name[player_name]
            if city_name not in blacklist_cities:
                city = WeLikeSportzCities.query.filter_by(cities_name=city_name).first()
                if city is not None:
                    user.players_cityid = city.id
                    user.players_teamid = team_name_to_teamid[user.players_team]
                    db.session.commit()

    for team_name in team_name_to_city_name:
        user = WeLikeSportzTeams.query.filter_by(teams_name=team_name).first()
        if user is not None:
            city_name = team_name_to_city_name[team_name]
            if city_name not in blacklist_cities:
                city = WeLikeSportzCities.query.filter_by(cities_name=city_name).first()
                if city is not None:
                    user.teams_cityid = city.id
                    db.session.commit()

    for player in WeLikeSportzPlayers.query.filter_by(players_youtubeid="NaN"):
        player.players_youtubeid = get_video_results(player.players_name)
        db.session.commit()
