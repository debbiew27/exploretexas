from googleapiclient.discovery import build
import json

YT_API_KEY = "AIzaSyBzQsMKap-89_XGnREMcuXFqaKlN5ln-80"
youtube = build("youtube", "v3", developerKey=YT_API_KEY)


def getYoutubeLink(player_name):
    request = youtube.search().list(
        part="snippet",
        order="relevance",
        q=player_name,
        type="video",
        videoDefinition="high",
    )
    response = request.execute()
    # print(json.dumps(response))
    videoID = response["items"][0]["id"]["videoId"]
    return videoID
    # print(response)


print(getYoutubeLink("LeBron James"))
