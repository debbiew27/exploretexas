from numpy import genfromtxt
from flask import Flask, render_template
import flask_restless
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import and_, or_, func, Integer
from flask import request, make_response, jsonify, send_from_directory
import math
from flask_marshmallow import Marshmallow
from marshmallow import fields, post_dump
from models import city, team, player
from models import app, db, city_schema, player_schema, team_schema

manager = flask_restless.APIManager(app, session=db.session)
# Create API endpoints, which will be available at /api/<tablename> by
# default. Allowed HTTP methods can be specified as well.
# Pagination through results_per_page.
manager.create_api(
    city,
    methods=["GET"],
    url_prefix="",
    collection_name="cities",
    results_per_page=100,
)
manager.create_api(
    team,
    methods=["GET"],
    url_prefix="",
    collection_name="teams",
    results_per_page=100,
)
manager.create_api(
    player,
    methods=["GET"],
    url_prefix="",
    collection_name="players",
    results_per_page=100,
)


def get_query(name, queries):
    try:
        return queries[name]
    except KeyError:
        return None


@app.route("/")
# @app.route("/<path:path>")
def hello_world():
    """
    Root route for our Flask app API.
    """
    return "We Like Sportz API provides the best-update-to-date information for Basketball, Soccer, Football, and Hockey!"


def search_players_query(q):
    """
    Input: query string "q"
    Output: List of Player instances that contain "q" insensitive to capital letters.
    """
    player_query = db.session.query(player)
    searches = []
    searches.append(player.players_name.ilike("%" + q + "%"))
    searches.append(player.players_city.ilike("%" + q + "%"))
    searches.append(player.players_state.ilike("%" + q + "%"))
    searches.append(player.players_sport.ilike("%" + q + "%"))
    searches.append(player.players_description.ilike("%" + q + "%"))
    searches.append(player.players_origin.ilike("%" + q + "%"))
    searches.append(player.players_height.ilike("%" + q + "%"))
    searches.append(player.players_weight.ilike("%" + q + "%"))
    searches.append(player.players_birthday.ilike("%" + q + "%"))
    searches.append(player.players_facebook.ilike("%" + q + "%"))
    searches.append(player.players_jerseynum.ilike("%" + q + "%"))
    searches.append(player.players_team.ilike("%" + q + "%"))
    searches.append(player.players_picture.ilike("%" + q + "%"))
    searches.append(player.players_youtubeid.ilike("%" + q + "%"))

    player_query = player_query.filter(or_(*tuple(searches)))
    return player_query


@app.route("/search_players", methods=["GET"])
def search_players():
    """
    Search API endpoint for players.
    """
    queries = request.args.to_dict(flat=False)
    page = get_query("page", queries)
    if page == None:
        page = 1
    else:
        # Convert the given page number into an int
        page = int(page[0])

    q = queries["q"][0]
    player_query = search_players_query(q)

    if page != -1:
        per_page = (
            int(get_query("results_per_page", queries).pop())
            if get_query("results_per_page", queries)
            else 20
        )
        players = player_query.paginate(page=page, per_page=per_page)
        result = player_schema.dump(players.items, many=True)
    else:
        result = player_schema.dump(player_query, many=True)
    return {
        "num_results": player_query.count(),
        "objects": result,
        "page": page,
        "total_pages": math.ceil(player_query.count() / per_page),
    }


def search_cities_query(q):
    """
    Input: query string "q"
    Output: List of Cities instances that contain "q" insensitive to capital letters.
    """
    cities_query = db.session.query(city)
    searches = []
    searches.append(city.cities_name.ilike("%" + q + "%"))
    searches.append(city.cities_popularsports.ilike("%" + q + "%"))
    searches.append(city.cities_state.ilike("%" + q + "%"))
    searches.append(city.cities_picture.ilike("%" + q + "%"))
    try:
        searches.append(city.cities_long == float(q))
        searches.append(city.cities_lat == float(q))
        searches.append(city.cities_numsports == int(q))
        searches.append(city.cities_population == int(q))
        searches.append(city.cities_numteams == int(q))
    except Exception as e:
        pass

    cities_query = cities_query.filter(or_(*tuple(searches)))
    return cities_query


@app.route("/search_cities", methods=["GET"])
def search_cities():
    """
    Search API endpoint for cities.
    """
    queries = request.args.to_dict(flat=False)
    page = get_query("page", queries)
    if page == None:
        page = 1
    else:
        # Convert the given page number into an int
        page = int(page[0])

    q = queries["q"][0]
    cities_query = search_cities_query(q)

    if page != -1:
        per_page = (
            int(get_query("results_per_page", queries).pop())
            if get_query("results_per_page", queries)
            else 20
        )
        cities = cities_query.paginate(page=page, per_page=per_page)
        result = city_schema.dump(cities.items, many=True)
    else:
        result = city_schema.dump(cities_query, many=True)

    return {
        "num_results": cities_query.count(),
        "objects": result,
        "page": page,
        "total_pages": math.ceil(cities_query.count() / per_page),
    }


def search_teams_query(q):
    """
    Input: query string "q"
    Output: List of Teams instances that contain "q" insensitive to capital letters.
    """
    teams_query = db.session.query(team)
    searches = []
    searches.append(team.teams_city.ilike("%" + q + "%"))
    searches.append(team.teams_league.ilike("%" + q + "%"))
    searches.append(team.teams_name.ilike("%" + q + "%"))
    searches.append(team.teams_sport.ilike("%" + q + "%"))
    searches.append(team.teams_stadium.ilike("%" + q + "%"))
    searches.append(team.teams_state.ilike("%" + q + "%"))
    searches.append(team.teams_badge.ilike("%" + q + "%"))
    searches.append(team.teams_banner.ilike("%" + q + "%"))
    searches.append(team.teams_logo.ilike("%" + q + "%"))
    searches.append(team.teams_facebook.ilike("%" + q + "%"))
    try:
        searches.append(team.teams_yearformed == int(q))
        searches.append(team.teams_stadiumcapacity == int(q))
    except Exception as e:
        pass
    teams_query = teams_query.filter(or_(*tuple(searches)))
    return teams_query


@app.route("/search_teams", methods=["GET"])
def search_teams():
    """
    Search API endpoint for teams.
    """
    queries = request.args.to_dict(flat=False)
    page = get_query("page", queries)
    if page == None:
        page = 1
    else:
        # Convert the given page number into an int
        page = int(page[0])

    q = queries["q"][0]
    teams_query = search_teams_query(q)

    if page != -1:
        per_page = (
            int(get_query("results_per_page", queries).pop())
            if get_query("results_per_page", queries)
            else 20
        )
        teams = teams_query.paginate(page=page, per_page=per_page)

        result = team_schema.dump(teams.items, many=True)
    else:
        result = team_schema.dump(teams_query, many=True)

    return {
        "num_results": teams_query.count(),
        "objects": result,
        "page": page,
        "total_pages": math.ceil(teams_query.count() / per_page),
    }


if __name__ == "__main__":
    db.init_app(app)
    app.run(host="0.0.0.0", port=5000, threaded=True, debug=True)
